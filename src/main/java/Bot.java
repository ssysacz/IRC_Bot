import com.google.common.base.Strings;
import handlers.CommandHandler;
import handlers.SimpleMsg;
import networking.IRCConnection;
import networking.ConnectionFactory;
import networking.exception.DCException;

import java.io.*;

/**
 * Created by Tomasz on 11/8/2015.
 */
public class Bot {

    IRCConnection connection;
    CommandHandler handler;
    private String ip;
    private int port;

    private void init(String ip, int port)throws IOException{
     connection = new IRCConnection(ConnectionFactory.getIRCConncetion(ip, port));
    }
    
    public Bot setIp(String ip){
        this.ip = ip;
        return this;
    }

    public Bot setPort(int port){
        this.port = port;
        return this;
    }

    public Bot connect()throws IOException{
        init(ip,port);
        handler = new CommandHandler();
        handler.addCommand(new SimpleMsg("!bot","Jestem botem"));
        return this;
    }

    public void startBot() throws Exception{
        connectToServer();
        joinServer();
        mainLoop();
    }

    private void joinServer() throws IOException{
        try {
            connection.joinChannel("hehe");
        } catch (DCException e) {
            e.printStackTrace();
            reconnect();
        }
    }

    private void connectToServer() throws IOException {
        try {
            connection.loginToServer("BringBot");
            String line;
            while ((line = connection.getMsg()) != null) {
                System.out.println(line);
                if (line.indexOf("004") >= 0) {
                    System.out.print("Zalogowany heheskzi !");
                    break;
                }
                if (line.toUpperCase().startsWith("PING ")) {
                    System.out.println("PING BITCH");
                    connection.pong(line);
                }
            }
        }catch (DCException e){
            e.printStackTrace();
            reconnect();
        }
    }

    private void reconnect() {
    }

    private void mainLoop() throws IOException {
        String line;
        String anwser;
        try {
            while(true){
                line = connection.getMsg();
                System.out.println(line);
                if (line.toUpperCase( ).startsWith("PING ")) {
                    connection.pong(line);
                }
                if(line.contains("PRIVMSG")){
                    anwser = handler.handleNewMsg(line);
                    if(!Strings.isNullOrEmpty(anwser))
                        connection.writeToChannel("hehe",anwser);
                }
            }
        } catch (DCException e) {
            e.printStackTrace();
            reconnect();
        }
    }
}
