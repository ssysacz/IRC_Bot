package utils;

/**
 * Created by Tomasz on 11/15/2015.
 */
public class MsgUtils {

    public static String getUsername(String serverMsg){
        return serverMsg.substring(1,serverMsg.indexOf("!~"));
    }

    public static String getMsg(String serverMsg){
        serverMsg = serverMsg.substring(serverMsg.indexOf("PRIVMSG"));
        serverMsg = serverMsg.substring(serverMsg.indexOf(":")+1);

        return serverMsg;
    }

    public static String getNickname(String serverMsg) {
        serverMsg = serverMsg.substring(serverMsg.indexOf("!~")+1);
        serverMsg = serverMsg.substring(1,serverMsg.indexOf("@"));
        return  serverMsg;
    }

    public static String getChannelName(String serverMsg){
        serverMsg = serverMsg.substring(serverMsg.indexOf("PRIVMSG")+8);
        serverMsg =  serverMsg.substring(1, serverMsg.indexOf(" "));
        return serverMsg;
    }
}
