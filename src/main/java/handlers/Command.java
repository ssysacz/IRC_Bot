package handlers;

/**
 * Created by Tomasz on 11/15/2015.
 */
public interface Command {

    public String handle(String serverMsg);

    public String getAbout();

    public String getCommand();
}
