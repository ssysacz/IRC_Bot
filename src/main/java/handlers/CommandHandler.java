package handlers;

import java.util.HashMap;

/**
 * Created by Tomasz on 11/15/2015.
 */
public class CommandHandler {
    private HashMap<String, Command> commands = new HashMap<String, Command>();

    public CommandHandler(){

    }

    public CommandHandler addCommand(Command command){
        commands.put(command.getCommand(), command);
        return this;
    }

    public String handleNewMsg(String serverMsg){
        for(String command : commands.keySet()){
            if(serverMsg.toLowerCase().contains(command))
                return commands.get(command).handle(serverMsg);
        }
        return null;
    }

}
