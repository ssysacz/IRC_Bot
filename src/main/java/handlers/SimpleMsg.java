package handlers;

/**
 * Created by Tomasz on 11/15/2015.
 */
public class SimpleMsg implements Command {
    private String command = "!bot";
    private String answer;

    public SimpleMsg(String command, String answer){
        this.command = command;
        this.answer = answer;
    }

    public String handle(String serverMsg) {
        if(serverMsg.toLowerCase().contains(command))
            return answer;
        return null;
    }

    public String getAbout() {
        return null;
    }

    public String getCommand() {
        return command;
    }
}
