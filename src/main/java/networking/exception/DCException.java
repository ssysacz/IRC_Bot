package networking.exception;

/**
 * Created by Tomasz on 11/29/2015.
 */
public class DCException extends Exception {

    public DCException(){
    }

    public DCException(String message){
        super(message);
    }

    public DCException(Throwable cause){
        super(cause);
    }

    public DCException(String message, Throwable cause){
        super(message,cause);
    }
}
