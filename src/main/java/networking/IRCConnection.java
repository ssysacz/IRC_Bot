package networking;

import networking.exception.DCException;

import java.io.IOException;

/**
 * Created by Tomasz on 11/15/2015.
 */
public class IRCConnection {
    private SocketConnection connection;
    private String endOfLine = "\r\n";

    public IRCConnection(SocketConnection connection){
        this.connection = connection;
    }

    public void writeToChannel(String channel, String message) throws DCException{
        write("PRIVMSG #" + channel + " :" + message);
    }

    public void writeToUser(String user, String message)throws DCException{
        write("PRIVMSG " + user + ": " + message);
    }

    public void loginToServer(String nick)throws DCException{
        nick = " " + nick;
        write("NICK"+nick);
        write("USER" + nick + nick + nick + " :" + nick);
    }

    public void pong(String serverMsg)throws DCException{
        write("PONG " + serverMsg.substring(5));
    }

    public String getMsg() throws DCException{
        return connection.getNextMessage();
    }

    public void joinChannel(String channelName) throws DCException{
        write("JOIN #" + channelName);
    }

    private void write(String msg) throws DCException{
        connection.write(msg+endOfLine);
    }
}
