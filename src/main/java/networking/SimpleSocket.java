package networking;

import networking.exception.DCException;

import java.io.*;
import java.net.Socket;

/**
 * Created by Tomasz on 11/9/2015.
 */
public class SimpleSocket implements SocketConnection {
    Socket ircConnection;
    BufferedReader reader;
    BufferedWriter writer;

    public SimpleSocket(String ip, int port){
        try {
            ircConnection = new Socket(ip,port);
            reader = new BufferedReader(new InputStreamReader(ircConnection.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(ircConnection.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getNextMessage() throws DCException {
        try {
            return reader.readLine();
        }catch (IOException e){
            throw new DCException();
        }
    }

    public void write(String msg) throws DCException{
        writeWithFlush(msg);
    }

    private void writeWithFlush(String msg) throws DCException{
        try{
            writer.write(msg);
            writer.flush();
        }catch (IOException e){
            throw new DCException();
        }
    }
}
