package networking;

import networking.exception.DCException;

import java.io.IOException;

/**
 * Created by Tomasz on 11/14/2015.
 */
public interface SocketConnection {

    public String getNextMessage() throws DCException;

    public void write(String plainMessage) throws DCException;

}
