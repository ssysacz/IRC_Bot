import org.junit.Test;
import utils.MsgUtils;

import static org.junit.Assert.*;

/**
 * Created by Tomasz on 11/28/2015.
 */
public class UtilsTests{

    @Test
    public void testUserName(){
        assertEquals("Getting username","linususus", MsgUtils.getUsername(":linususus!~Lasdfin@89-70-206-199.dynamic.chello.pl PRIVMSG #hehe :asdfasdfasdf"));
        assertEquals("","BringBot",MsgUtils.getUsername(":BringBot!~BringBot@89-70-206-199.dynamic.chello.pl JOIN #hehe"));
    }

    @Test
    public void testMsg(){
        assertEquals("Getting msg", "asdfasdfasdf", MsgUtils.getMsg(":linususus!~Lasdfin@89-70-206-199.dynamic.chello.pl PRIVMSG #hehe :asdfasdfasdf"));
    }

    @Test
    public void testNickname(){
        assertEquals("Getting msg", "Lasdfin", MsgUtils.getNickname(":linususus!~Lasdfin@89-70-206-199.dynamic.chello.pl PRIVMSG #hehe :asdfasdfasdf"));
    }

    @Test
    public void testChannelName(){
        assertEquals("Getting channelname", "hehe", MsgUtils.getChannelName(":linususus!~Lasdfin@89-70-206-199.dynamic.chello.pl PRIVMSG #hehe :asdfasdfasdf"));

    }
}
